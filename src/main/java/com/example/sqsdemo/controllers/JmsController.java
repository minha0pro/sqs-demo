package com.example.sqsdemo.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/jms")
public class JmsController {

    private Logger logger = LoggerFactory.getLogger(JmsController.class);

    @Autowired
    private JmsTemplate jmsTemplate;

    @PostMapping("/messages")
    public String sendMessage(@RequestParam("queue") String queue,
                              @RequestParam("message") String message) {
        try {
            jmsTemplate.convertAndSend(queue, message);
            return "OK";
        } catch (JmsException e) {
            logger.error("JMS SEND ERROR: " + e.getMessage());
            return "ERROR: " + e.getMessage();
        }
    }
}
