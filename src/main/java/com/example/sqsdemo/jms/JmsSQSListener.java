package com.example.sqsdemo.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;

@Component
public class JmsSQSListener {

    private Logger logger = LoggerFactory.getLogger(JmsSQSListener.class);

    @JmsListener(destination = "${aws.queue}")
    public void receive(String message) throws JMSException {
        logger.info("Received message {}", message);
        // do something useful with the message here
    }
}
